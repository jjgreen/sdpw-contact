#!/bin/sh

BASE=$1
XMAX=$2
YMAX=$3
GRD="$BASE.grd"
PS="$BASE-hist.ps"

gmt grd2xyz $GRD | \
    awk '{print $3}' | \
    gmt pshistogram \
	-Bxa5f1+l"iterations" \
	-Bya20f10+l"frequency (%)" \
	-BWSne+t"Newton-Raphson"+glightblue \
	-Lthin \
	-JX3i/3i \
	-W1 \
	-Z1 \
	-Gorange \
	-R0/$XMAX/0/$YMAX > $PS

gmt psconvert -A -P -Tf $PS
rm $PS
