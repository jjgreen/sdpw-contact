koch.grd: Title: koch.grd
koch.grd: Command: xyz2grd /tmp/sdpw-koch.dat -Gkoch.grd -I400+/400+ -R-1.00000000/1.00000000/-1.00000000/1.00000000
koch.grd: Remark: 
koch.grd: Gridline node registration used [Cartesian grid]
koch.grd: Grid file format: nf = GMT netCDF format (32-bit float), COARDS, CF-1.5
koch.grd: x_min: -1 x_max: 1 x_inc: 0.00501253132832 name: x nx: 400
koch.grd: y_min: -1 y_max: 1 y_inc: 0.00501253132832 name: y ny: 400
koch.grd: z_min: 0 z_max: 57 name: z
koch.grd: scale_factor: 1 add_offset: 0
koch.grd: 77414 nodes (48.4%) set to NaN
koch.grd: mean: 17.5183687308 stdev: 10.8886845583 rms: 20.6265668195
koch.grd: format: netCDF-4 chunk_size: 134,134 shuffle: on deflation_level: 3
