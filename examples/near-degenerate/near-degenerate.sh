#!/bin/sh

RNG='-R0/1/0/0.09'
PRJ='-JX3i/3i'

APEN='-W0.75p,black'
LPEN='-W1.50p,black'

PS='near-degenerate.ps'

gmt psxy fABinf.dat $LPEN $PRJ $RNG -K > $PS

for n in '004' '008' '016' '032' '064' '128'
do
    gmt psxy fAB${n}.dat $APEN $PRJ $RNG -O -K >> $PS
done

gmt psbasemap -Bfa0.2/0.01neWS $RNG $PRJ -O >> $PS
gmt psconvert -A -P -Tf $PS
rm $PS
