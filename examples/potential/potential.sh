#!/bin/sh

set -e

BASE=$1
ALINE=$2

RNG='-R-1/1/-1/1'
PRJ='-JX3i/3i'

CPEN='-W0.5p,grey'
APEN='-W0.75p,black'
LPEN='-W1.50p,black'
WPEN='-W3p,white'

DOT='-W0.75p -Gwhite -Sc0.125c'

PS="$BASE.ps"

DOTFONT="7,Courier-Bold"
DOTOUTLINE="$DOTFONT,-=2.5p,white"

# create the hair-shifted grd file (a kludge to get GMT to plot
# the "zero countour")

gmt grdmath $BASE.grd 1e-8 SUB = hair.grd

# 0.1 contours in light grey

gmt grdcontour hair.grd $CPEN -C0.1 $RNG $PRJ -K > $PS

# 0.5 contour labels dumped to file, contour drawn without
# labels, labels drawn by pstext

LABELS="/tmp/sdpw-$BASE-labels.dat"
gmt grdcontour hair.grd -Gl$ALINE -A1+t$LABELS $RNG $PRJ -K -O > /dev/null
gmt grdcontour hair.grd $APEN -C0.5 $RNG $PRJ -K -O >> $PS
gmt pstext $LABELS -Dj4p -F+a+f$DOTOUTLINE+jMC $PRJ $RNG -K -O >> $PS
gmt pstext $LABELS -Dj4p -F+a+f$DOTFONT+jMC $PRJ $RNG -K -O >> $PS
rm $LABELS

# the boundary



gmt psxy $BASE-lines.dat $WPEN $PRJ $RNG -K -O >> $PS
gmt psxy $BASE-lines.dat $LPEN $PRJ $RNG -K -O >> $PS
gmt psxy $BASE-lines.dat $DOT $PRJ $RNG -K -O >> $PS

# basemap

gmt psbasemap -B0 $RNG $PRJ -O >> $PS

# output

gmt psconvert -C-dCompatibilityLevel=1.5 -A -P -Tf $PS
rm hair.grd $PS
