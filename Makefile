default: all

include Common.mk

all: build

build:
	$(PYTHON) setup.py build

TEST_DEPENDS = pytest pytest-cov numpy hypothesis setuptools

install-test-depends:
	$(PIP) install $(TEST_DEPENDS)

test:
	$(PYTHON) -m pytest

PYCODESTYLE_DEPENDS = pycodestyle

install-pycodestyle-depends:
	$(PIP) install $(PYCODESTYLE_DEPENDS)

pep8: pycodestyle

pycodestyle:
	pycodestyle sdpw test

coverage-view:
	xdg-open test/coverage/index.html

clean:
	$(RM) .coverage
	rm -rf build .eggs sdpw_contact.egg-info \
		sdpw/__pycache__ test/__pycache__

.PHONY: test build
