from sdpw.ellipse import Ellipse
from sdpw.sdpw import fAB
from pytest import approx
import numpy as np
import pytest


# the function fAB in the non-degenerate case

@pytest.fixture
def A():
    return Ellipse(1, 1, 0)


@pytest.fixture
def B():
    return Ellipse(1, 1, 0)


def test_extrema_disjoint(A, B):
    rAB = np.array([3, 0])
    assert fAB(A, B, rAB, 0) == approx(0)
    assert fAB(A, B, rAB, 1) == approx(0)


def test_extrema_touching(A, B):
    rAB = np.array([2, 0])
    assert fAB(A, B, rAB, 0) == approx(0)
    assert fAB(A, B, rAB, 1) == approx(0)


def test_extrema_intersecting(A, B):
    rAB = np.array([1, 0])
    assert fAB(A, B, rAB, 0) == approx(0)
    assert fAB(A, B, rAB, 1) == approx(0)


# By symmetry, fAB it attains its maximum at 1/2, so we can assert
# the properties of the PW-contact function

def test_midpoint_disjoint(A, B):
    rAB = np.array([3, 0])
    assert fAB(A, B, rAB, 0.5) > 1.0


def test_midpoint_touching(A, B):
    rAB = np.array([2, 0])
    assert fAB(A, B, rAB, 0.5) == approx(1.0)


def test_midpoint_intersecting(A, B):
    rAB = np.array([1, 0])
    assert fAB(A, B, rAB, 0.5) < 1.0
